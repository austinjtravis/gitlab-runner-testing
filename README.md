# gitlab-runner-testing
I made this to see if I could run stages of a gitlab pipeline locally.  This fixes the issue of having to push up to the repository to see if changes worked.  Looking at gitlab docs it seemed possible to run a pipeline locally using docker.  

## Requirements
* WSL 2  
   - I use ubuntu
* Docker  
   - I use Docker Desktop on windows  
   - Best to create a Dockerhub account for managing images, but not required  

# Create a local gitlab runner  
1. Install runner  
    - I used docker, but there are many other options (windows, linux, ... installs)  
    - I followed this guide and used option 1 to create a container for the runner: https://docs.gitlab.com/runner/install/docker.html#install-the-docker-image-and-start-the-container.  
    - Note anytime they mention a GitLab instance URL just use `gitlab.com`.  
        - Different if your gitlab server has different host name  

2. Register the runner  
    - I followed this guide since I used docker, but they have guides for all installs: https://docs.gitlab.com/runner/register/index.html#docker  
    - Under Settings/CI-CD expand the Runners tab  
        - This is where your registration token will be
    - for default image pick it based on your repo.  I went with `python:3.10`.   
        - Can pick anyt image on dockerhub  

3. Run pipeline on container  
The main command to run a gitlab runner locally is `gitlab-runner exec`.  See https://docs.gitlab.com/runner/commands/#gitlab-runner-exec for all options.  Since the gitlab runner is installed on the docker container, we just need to execute a command on our container.  To do this we can make use of `docker exec`:   

```  
docker exec -it -w $PWD gitlab-runner gitlab-runner exec docker test
```  
The structure of this command is   

```  
docker exec --docker-options container-name command
```  
In the case above  

```  
docker exec -it -w $PWD gitlab-runner gitlab-runner exec docker test
(a)         (b)  (c)    (d)           (e)                (f)    (g)
```
* **(a)**: Command to run command in our container  
* **(b)**: Flag to turn on interactive mode  
   - Can open a terminal directly inside container using Docker desktop  
* **(c)**: Set the container to run in our current directory  
* **(d)**: Container name  
*Note*: Everything after here is the command we want to run inside the container 
* **(e)**: `gitlab-runner` command  (`gitlab-runner exec`)
* **(f)**: What we're using to execute the gitlab runner command  
* **(g)**: The name of the stage we want to run in our `.gitlab-ci.yml` file  
   - in this repo it only has a single stage named `test`

When running this make sure your current working directory is at the root of the repository.  I found this post very helpful: https://stackoverflow.com/a/65920577  

After running `` the output is:  
```  
Runtime platform                                    arch=amd64 os=linux pid=197 revision=de104fcd version=14.5.1
Running with gitlab-runner 14.5.1 (de104fcd)
Preparing the "docker" executor
Using Docker executor with image python:3.10 ...
Pulling docker image python:3.10 ...
Using docker image sha256:47ebea899258a88784a89a2826cd932fff5a978dddc95615694db699f0de472b for python:3.10 with digest python@sha256:1aa28c1073244b1211fcf223748c18b451890b5b95edcc2e441857ee10a8d583 ...
Preparing environment
Running on runner--project-0-concurrent-0 via a158804021fe...
Getting source from Git repository
Fetching changes...
Initialized empty Git repository in /builds/project-0/.git/
Created fresh repository.
Checking out bebea0da as main...

Skipping Git submodules setup
Executing "step_script" stage of the job script
Using docker image sha256:47ebea899258a88784a89a2826cd932fff5a978dddc95615694db699f0de472b for python:3.10 with digest python@sha256:1aa28c1073244b1211fcf223748c18b451890b5b95edcc2e441857ee10a8d583 ...
$ ls -a
.
..
.git
.gitlab-ci.yml
README.md
Job succeeded
```  
and if we go into the `.gitlab-ci.yml file we see:  
```  
image: python:3.10
stages:
- test
test:
  stage: test
  script: 
    - ls -a
```  
which shows we executed the contents of `test` stage!  For further verification we can look at the latest pipeline and verify this output matches this: https://gitlab.com/austinjtravis/gitlab-runner-testing/-/jobs/1847833477 

3.1 Mounting code directory to docker container  
The container we created earlier has no access to the code in our repository.  To fix this we can mount a volume (add a directory) inside our contaaianer.  
``` 
docker run -d --name gitlab-runner --restart always -v $PWD:$PWD -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
```  
The `-v` command is shorthand for `--volume` and my understanding is it just adds files into the container  

# Video demonstration  
Here's a quick demonstration of what running the command looks like: https://www.youtube.com/watch?v=6hNicsDAgT8 (also you can check out my medocre osu plays while you're there)

## TODOs  
* The method I described only runs single stages at a time.  
    - Need to find way to run everything in one shot  
* Need to figure out how to run this with prebuilt docker images  

